#!/bin/bash

# --------------------------------------------------------------
# Smart Garden CLI script
# @author Hervé Perchec <herve.perchec@gmail.com>
# --------------------------------------------------------------

# Get entire path of the script directory
SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd $SCRIPT_DIR
SCRIPT_DIR=$PWD

# Get environment variables from .env file in parent directory
source ../.env

# Init COMMAND to empty string
COMMAND=""
# Init COMMAND_ARGS to empty string
COMMAND_ARGS=""

# Defines the verbosity level
# Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH', default: 'HIGH'
VERBOSE_LEVEL=HIGH

# Init exit code, default: 0 (Success)
EXIT_CODE=0

# Declare ON/OFF constants
# INFO: for relay module, 
# 0 is 'ON' and 1 is 'OFF'
ON=0
OFF=1

# Channels
declare -A CHANNELS
CHANNELS[1]=$RELAY_PIN_1
CHANNELS[2]=$RELAY_PIN_2
CHANNELS[3]=$RELAY_PIN_3
CHANNELS[4]=$RELAY_PIN_4

# RGB Led Strip pins value
RLS_PWM_COLOR_RED=0
RLS_PWM_COLOR_GREEN=255
RLS_PWM_COLOR_BLUE=160

# Show header function
show_header() {
    echo -e "---------------------------------------------------------------"
    echo
    echo -e "\e[32m                                            oxkd    \e[0m"
    echo -e "\e[32m                                     ooddxk0XNNKd   \e[0m"
    echo -e "\e[32m                               oxkO0KKXXNWWMMMMNko  \e[0m"
    echo -e "\e[32m    ____ _     ___          ok0XNWWMMMMMMM WWWMN0d  \e[0m"
    echo -e "\e[32m   / ___| |   |_ _|        d0NMMMMMMMMMW/  NWMMN0d  \e[0m"
    echo -e "\e[32m  | |   | |    | |       d0WMMMMWWN/     /WMMMNko   \e[0m"
    echo -e "\e[32m  | |___| |___ | |      okXWWWX/     /XWMMMMMWK/    \e[0m"
    echo -e "\e[32m   \____|_____|___|     xKXX/     /WWWMMMMMMW/      \e[0m"
    echo -e "\e[32m                        xK/   oWMMMMMMMMMM/         \e[0m"
    echo -e "\e[32m                            oNWWWWMWWF              \e[0m"
    echo -e "\e[32m                         oNKOX*                     \e[0m"
    echo -e "\e[32m                        d0XKK                       \e[0m"
    echo
    echo -e "  SMART GARDEN"
    echo
    echo -e "  @author Hervé Perchec <herve.perchec@gmail.com>"
    echo
    echo -e "---------------------------------------------------------------"
    echo
}

# Show help function
show_help() {
    echo -e "Usage: cli.sh \e[33m[OPTIONS]\e[0m \e[36mCOMMAND\e[0m [...<args>]"
    echo
    echo -e "\e[33mOPTIONS:\e[0m"
    echo
    echo -e "  \e[33m-h\e[0m       Show this help."
    echo -e "  \e[33m-v\e[0m       Defines the verbose level. Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH' (Default: 'HIGH')."
    echo
    echo -e "\e[36mCOMMAND:\e[0m"
    echo
    echo -e "  \e[36minit\e[0m                        Initialize GPIO ports."
    echo -e "  \e[36mturn\e[0m <channel> on|off       Take control of GPIO ports."
    echo -e "  \e[36mstatus\e[0m <channel>            Get GPIO logical value of channel ('ON' or 'OFF')."
    echo
}

# Parse command
# Take full command as parameters
parse_command() {
    # Get the command
    COMMAND=$1
    # Get args (all other args)
    COMMAND_ARGS=${@:2}

    case $COMMAND in
        "init")
            to_exec () {
                echo "Initialization..."
                echo
                # CHANNEL 1
                echo -e "\e[1;33m- CHANNEL 1:\e[0m"
                init_channel_gpio_port ${CHANNELS[1]}
                echo
                # CHANNEL 2
                echo -e "\e[1;33m- CHANNEL 2:\e[0m"
                init_channel_gpio_port ${CHANNELS[2]}
                echo
                # CHANNEL 3
                echo -e "\e[1;33m- CHANNEL 3:\e[0m"
                init_channel_gpio_port ${CHANNELS[3]}
                echo
                # CHANNEL 4
                echo -e "\e[1;33m- CHANNEL 4:\e[0m"
                init_channel_gpio_port ${CHANNELS[4]}
                echo
                # RGB Led Strip RED
                echo -e "\e[1;33m- RGB Led Strip RED pin:\e[0m"
                init_rls_gpio_port $RLS_PIN_RED $RLS_PWM_COLOR_RED
                echo
                # RGB Led Strip GREEN
                echo -e "\e[1;33m- RGB Led Strip GREEN pin:\e[0m"
                init_rls_gpio_port $RLS_PIN_GREEN $RLS_PWM_COLOR_GREEN
                echo
                # RGB Led Strip BLUE
                echo -e "\e[1;33m- RGB Led Strip BLUE pin:\e[0m"
                init_rls_gpio_port $RLS_PIN_BLUE $RLS_PWM_COLOR_BLUE
                echo
            }
            ;;
        "turn")
            to_exec () {
                turn_channel $COMMAND_ARGS
            }
            ;;
        "status")
            to_exec () {
                get_channel_status $COMMAND_ARGS
            }
            ;;
        *)
            show_header
            echo -e "\e[31mUnknown command\e[0m"
            echo
            show_help
            EXIT_CODE=1 # Exit code: 1 (Error)
            quit
            ;;
    esac
}

# Quit function
quit() {
    exit $EXIT_CODE
}

# Get GPIO port logical value
# @param {int} $1 - GPIO BCM number
get_gpio_value() {

    # Get first arg -> GPIO BCM number
    local _gpio_number=$1

    local _logical_value=$(gpio -g read $_gpio_number)

    echo $_logical_value
}

# Translate logical value to string (1: 'OFF', 0: 'ON')
# @param {int} $1 - logical value (1 or 0)
logical_value_to_str() {

    # Get first arg -> boolean value
    local _logical_value=$1

    # Must be 0 and 1
    if [ $_logical_value -eq $ON ]; then
        echo 'ON'
    elif [[ $_logical_value -eq $OFF ]]; then
        echo 'OFF'
    else
        echo -e "\e[31mError: logical_value_to_str() function argument 1 must be a boolean value: 0 or 1\e[0m"
        echo
        EXIT_CODE=1 # Exit code: 1 (Error)
        quit
    fi
}

# Init GPIO port to 'out' mode and turn off
# @param {int} $1 - GPIO BCM number
init_channel_gpio_port() {

    # Get first arg -> GPIO BCM number
    local _gpio_number=$1

    # GPIO mode is 'IN' by default
    # WARN: Turn off before setting 'out' mode to not send any electronic signal when switching to 'out' mode
    # because relay module is ON when GPIO logic value is 0
    echo -ne "Turn \e[1mOFF\e[0m GPIO (BCM) \e[1m$_gpio_number\e[0m:                 "
    # Turn OFF GPIO (BCM) $_gpio_number (-g option to use BCM numbers)
    gpio -g write $_gpio_number $OFF
    if [ $? -ne 0 ]; then
        echo -e "\e[31mfailed\e[0m"
        EXIT_CODE=1
        quit
    fi
    echo -e "\e[32msuccess\e[0m"

    # Check if GPIO port is OFF
    local _check_value=$(get_gpio_value $_gpio_number)
    if [ $_check_value -ne $OFF ]; then
        EXIT_CODE=1
        quit
    fi

    echo -ne "Set \e[1mOUT\e[0m mode for GPIO (BCM) \e[1m$_gpio_number\e[0m:         "

    # Set OUT mode for GPIO (BCM) $_gpio_number (-g option to use BCM numbers)
    gpio -g mode $_gpio_number OUT
    if [ $? -ne 0 ]; then
        echo -e "\e[31mfailed\e[0m"
        EXIT_CODE=1
        quit
    fi
    echo -e "\e[32msuccess\e[0m"
}

# Turn channel X to ON/OFF
# @param {int} $1 - Channel number (from 1 to 4)
# @param {string} $2 - 'ON' or 'OFF' (case insensitive)
turn_channel() {

    local _error=0

    # Get first arg -> channel number
    local _channel=$1
    # Must be a number between 1 and 4
    if [[ $_channel =~ ^[0-9]+$ ]] && [[ $_channel -ge 1 && $_channel -le 4 ]]; then
        local _gpio_number=${CHANNELS[$_channel]}
    else
        echo -e "\e[31mError: turn_channel() function argument 1 must be a channel number between 1 and 4\e[0m"
        echo
        local _error=1
    fi

    # Get second arg -> value
    local _on_off=$2
    # Check if param is set
    if [ -z ${_on_off} ]; then
        echo -e "\e[31mError: turn_channel() function argument 2 missing\e[0m"
        echo
        local _error=1
    else
        # Must be 'ON' or 'OFF' (case insensitive)
        if [[ $_on_off =~ ^[Oo][Nn]$ ]]; then
            local _logic_value=$ON
            local _on_off_str="ON"
        elif [[ $_on_off =~ ^[Oo][Ff][Ff]$  ]]; then
            local _logic_value=$OFF
            local _on_off_str="OFF"
        else
            echo -e "\e[31mError: turn_channel() function argument 2 must be 'ON' or 'OFF'\e[0m"
            echo
            local _error=1
        fi
    fi

    # If error -> show usage and exit
    if [ $_error -eq 1 ]; then
        echo -e "Usage : cli.sh turn \e[36m<channel>\e[0m \e[36mon|off\e[0m"
        echo -e " - \e[36m<channel>\e[0m : integer between 1 and 4"
        echo -e " - \e[36mon|off\e[0m    : string 'ON' or 'OFF' (case insensitive)"
        echo
        EXIT_CODE=1 # Exit code: 1 (Error)
        quit
    fi

    # No error -> set GPIO port to target value
    echo -ne "Turn \e[1m$_on_off_str\e[0m channel \e[1m$_channel\e[0m (GPIO (BCM) \e[1m$_gpio_number\e[0m): "

    gpio -g write $_gpio_number $_logic_value

    if [ $? -ne 0 ]; then
        echo -e "\e[31mfailed\e[0m"
        echo
        EXIT_CODE=1
        quit
    fi
    echo -e "\e[32msuccess\e[0m"
    echo

    # Check if GPIO port is target value
    local _check_value=$(get_gpio_value $_gpio_number)
    if [ $_check_value -ne $_logic_value ]; then
        echo -e "\e[31mAn error has occurred when turning GPIO (BCM) $_gpio_number to $_on_off_str \e[0m"
        echo
        EXIT_CODE=1
        quit
    fi
}

# Get channel status
# @param {int} $1 - Channel number (from 1 to 4)
get_channel_status() {

    local _error=0

    # Get first arg -> channel number
    local _channel=$1
    # Must be a number between 1 and 4
    if [[ $_channel =~ ^[0-9]+$ ]] && [[ $_channel -ge 1 && $_channel -le 4 ]]; then
        local _gpio_number=${CHANNELS[$_channel]}
    else
        echo -e "\e[31mError: get_channel_status() function argument 1 must be a channel number between 1 and 4\e[0m"
        echo
        local _error=1
    fi

    # If error -> show usage and exit
    if [ $_error -eq 1 ]; then
        echo -e "Usage : cli.sh status \e[36m<channel>\e[0m"
        echo -e " - \e[36m<channel>\e[0m : integer between 1 and 4"
        echo
        EXIT_CODE=1 # Exit code: 1 (Error)
        quit
    fi

    # No error -> get GPIO port logical value
    local _logical_value=$(get_gpio_value $_gpio_number)
    local _status=$(logical_value_to_str $_logical_value)
    echo $_status
}

# Init GPIO port to PWM mode and set value for LED color
# @param {int} $1 - GPIO BCM number
# @param {int} $2 - PWM value to set (from 0 to 255)
init_rls_gpio_port() {

    # Get first arg -> GPIO BCM number
    local _gpio_number=$1
    # Second arg -> PWM value
    local _pwm_value=$2

    # Use pigpio lib to set software PWM mode on target GPIO port
    # See also: https://abyz.me.uk/rpi/pigpio/pigs.html#P/PWM
    echo -ne "Set PWM value \e[1m$_pwm_value\e[0m on GPIO (BCM) \e[1m$_gpio_number\e[0m:     "
    pigs p $_gpio_number $_pwm_value
    if [ $? -ne 0 ]; then
        echo -e "\e[31mfailed\e[0m"
        EXIT_CODE=1
        quit
    fi
    echo -e "\e[32msuccess\e[0m"
}

# --------------------------------------------------------------
# END FUNCTIONS DECLARATIONS
# --------------------------------------------------------------

# Get options (MUST BE THE FIRST ARGUMENTS)
# -h : To show command help
# -v : To define verbose level
while getopts "hv:" opt
do
    case $opt in
        v) 
            case $OPTARG in
                "NONE") VERBOSE_LEVEL=${OPTARG} ;;
                "LOW") VERBOSE_LEVEL=${OPTARG} ;;
                "MEDIUM") VERBOSE_LEVEL=${OPTARG} ;;
                "HIGH") VERBOSE_LEVEL=${OPTARG} ;;
                *) 
                    show_header
                    echo -e "\e[31mInvalid value for -v option\e[0m"
                    echo
                    show_help
                    EXIT_CODE=1 # Exit code: 1 (Error)
                    quit
                    ;;
            esac
            ;;
        h) 
            show_header
            show_help
            quit
            ;;
        *)
            show_header
            show_help
            EXIT_CODE=1 # Exit code: 1 (Error)
            quit
            ;;
    esac
done

shift $((OPTIND-1)) # remove the processed options

# --------------------------------------------------------------
# END SCRIPT OPTIONS PARSING
# --------------------------------------------------------------

# Parse command
parse_command $@

# --------------------------------------------------------------
# END SCRIPT ARGS PARSING
# --------------------------------------------------------------

# Call show_header
if [ $VERBOSE_LEVEL == "HIGH" ]; then
    show_header
fi

# Display command name
if [ $VERBOSE_LEVEL == "HIGH" ]; then
    echo -e "COMMAND: \e[33m$COMMAND\e[0m"
    echo
fi

# --------------------------------------------------------------
# MAIN FUNCTION
# --------------------------------------------------------------

main() {

    #
    # 1 - Do stuff based on command passed
    #

    # Exec built command
    to_exec
    EXIT_CODE=$?

}

# --------------------------------------------------------------

# Call main()
main

# Exit with success
quit
