# 🍃 *Smart Garden* (CLI) 🌐

[![smart-garden](https://img.shields.io/static/v1?label=&message=Smart%20Garden&color=191919&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACkElEQVQ4T3VTTUhUURQ+Z37fjFpMmFmr3LZuU4sKyqKgRWZ/IES1MIICNRlTsgchpowWA0ltLFKohqJ9EGVB1KZNFAVqfxIWVI5vft57c+89nfucNw1iFw7vncv5vvOdn4vwnzNG73cRFEcFWMoGqyuJ+5+sFIrLL+/QfJMLdqoEVgsb+CYg90hA4byJZ2arMRWCDFGtA9YFAfnOEiwaPtCFRWAwE+UAyHEiUl4LhNyBJCYtTeQR3CM6qkCMMHADWzmr/9UqciChsBBSIm1I1R+Tcp6t+2RtzyTeJAqvArI5IKCzLRH44NwPVvSCCZ4TZO+b2P0z7YzMGlI2xaVw2+p6o5gmitZDyfaBZcnPJNi9HbDzFSBSdc037OGMIdShuJR0eHVfALn2CNfu+ASccSoE2NyOm0s+0CQzbqJZ0P54fuh2VMrjTAAHEhcRb9EnA6Gm6MsWUNxxDrdO6WCTrjcCqE38+8HEs9/13UTuypQhxDZNsG/NpSUFFsxVKbCbHPimXFCnFNhvTTj9kFvtlTGRH1gflPiFweG4kNBcD0GPYAGmywSWHtk4N+2PA9l+E9s92d4hwMn84F1DyCM6uyaghkCYCd5FfoNy3PLScA+GktjSU924sYXBRDwSTEdLpTYPXLaslTXQJAqthTdaQaAEhccK5lsdEK+JnK+GUjM8snVsuxlU52fmEUKUSH399THmLVKaXh5zIZuSkJuxYXGa38CJkJLAQOCF8czPWqN9JecQsWtLw+VMZZVNytSGwe3hHnQiuTFeWQ5U/8BCaL/I4JQjC0N7GlP5yipX12vS1Y0RwmFDUiuPCyvZlXwQAtW9N2F+XvExLX+V6eLw9hjBKBMEY0J1HEz0PV0eo/2/bjSnwFQoTgQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/smart-garden)
[![cli](https://img.shields.io/static/v1?labelColor=191919&label=CLI&message=v1.0.0&color=191919&logo=data:image/ico;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD///8S////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8S////v////9/////f////3////9/////f////3////9/////f////3////9/////f////3////9/////f////v//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////+////j////4////+P////j/////7//////////////////////////////////////////////////////////v///4////+P////j////4/////+///////////////////////////////////////////////////////////////////////////////////////////////A////4f///+D////g////4P///+D////g////4P///+D////g////4P///+D////g////4P///+H////A////D////x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////D////w////8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///w/////A////4f///+D////g////4P///+D////g////4P///+D////g////4P///+D////g////4P///+H////A//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7///+P////j////4////+P/////v/////////////////////////////////////////////////////////+////j////4////+P////j/////7//////////////////////////////////////////////////////////////////////////////////////////////7/////f////3////9/////f////3////9/////f////3////9/////f////3////9/////f////3////7////8S////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==)](https://gitlab.com/hperchec/smart-garden/cli)

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

**Table of contents**:

[[_TOC_]]

## Requirements

- GNU bash, version 5.0.3

## Get started

> **IMPORTANT**: Please refer to the [project documentation](https://gitlab.com/hperchec/smart-garden/docs).

```bash
git clone https://gitlab.com/hperchec/smart-garden/cli.git
```

## Commands

An all-in-one command is provided: `./cli.sh` (type `./cli.sh -h` to display help).

> **NOTE**: the `smart-garden` command is an alias for the `/smart-garden/cli/cli.sh` bash script configured in the `.bashrc` file.

### Usage / Help

```bash
./cli.sh -h
# OR
smart-garden -h
```

### Init GPIO ports

```bash
./cli.sh init
# OR
smart-garden init
```

### Turn on/off channel

```bash
./cli.sh turn <channel> on|off
# OR
smart-garden turn <channel> on|off
```

**Examples**:

```bash
# Turn on channel 1
./cli.sh turn 1 on
# OR
smart-garden turn 1 on
```

### Turn on/off channel

```bash
./cli.sh turn <channel> on|off
# OR
smart-garden turn <channel> on|off
```

**Examples**:

```bash
# Turn on channel 1
./cli.sh turn 1 on
# OR
smart-garden turn 1 on
```

----

Made with ❤ by [**Hervé Perchec**](http://herve-perchec.com/)